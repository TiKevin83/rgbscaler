# RGBScaler

A dedicated site for the canvas hack needed to play HTML5 compatible videos with scaling algorithms appropriate for pixel art

intended for use among the TASVideos and RetroRGB communities for web playback of retro game footage

## Summary

The rgbscaler.com canvas video player demonstrates a viable method for playing back pixel art footage from retro game consoles at larger sizes in web browsers. Through storing lossless footage from retro game consoles at very small sizes with full chroma and before PAR correction, archives of game footage can be kept very small and still displayed their sharpest on the web.

Please someone go implement the `image-rendering` tag for video in web standards and/or Chrome so that these hacks aren't necessary!

## React Library

The technology on this site is implemented as a React library that can be included in any React app - information on how to configure it is available in the Readme for the library.

https://github.com/TiKevin83/react-rgbscaler

## Site Usage

- Use uncompressed source retro/pixel art game footage at small original resolutions (typically 240p and smaller, but larger source resolutions can be used):
- Encode to lossless video for web playback using ffmpeg - this only works in Desktop Chrome
- `ffmpeg -i source.avi -c:v libx264 -crf 0 -c:a aac -b:a 320k -pix_fmt yuv444p10le output.mp4`
- Encode a higher compatibility version with minimal loss for use in other browsers and iOS/android
- `ffmpeg -i source.avi -vf scale=iw*2:ih*2:flags=neighbor+accurate_rnd+full_chroma_int+full_chroma_inp+bitexact -c:v libx264 -profile:v baseline -crf 1 -c:a aac -b:a 320k -pix_fmt yuv420p output-2x.mp4`
- Select the file from the "Choose File" button
- Choose display method, default "Integer (Window)"
    - Integer may be more sharp than Max, but doesn't help with non-square PAR
    - Fullscreen methods hide the controls
- Specify a Pixel Aspect Ratio in the PAR input for aspect ratio correction
- Start playback

## Background

### CSS is lagging on Pixel Art

Video footage with pixel art, including footage from many retro video game consoles, is difficult to display effectively in the browser. The HTML5 `video` element is not capable of being scaled with any methods appropriate for pixel art using CSS except in Firefox, unlike `img` elements which have a css property `image-rendering` which when set to `pixelated` or `crisp-edges` depending on the browser will force a nearest neighbor scaling algorithm for the image, which is at least passable at very large scale multiples. The `crisp-edges` value is supposed to use an algorithm similar to OBS's "area" filter which better preserves the shape of scaled objects, but it is implemented as a nearest neighbor algorithm in Firefox and unimplemented in Chrome.

### RGBScaler's canvas hack

However, there is an extreme hack to circumvent this possible with the `canvas` element. Data from a video element can be copied to a WebGL canvas, where custom shader logic can be used to implement the same algorithm used by OBS's `area` shader. A comparison is shown below of the visual difference between scaling a video element in Chrome and the hack demonstrated by rgbscaler when in Integer mode.

Basic Video Element        | RGBScaler
:-------------------------:|:-------------------------:
![](Blurry.png)  |  ![](Sharp.png)

### Bandwidth and quality considerations

An alternative method commonly used for playing back pixel art on the web is to prescale it to 1080p or 4k for uploading on sites like YouTube. This has several problems. First, Youtube only uses 4:2:0 chroma subsampling, the practical effect of which is that to preserve pixel art on YouTube it has to be scaled to at least 2x width and height. YouTube also only allows 60fps if the uploaded video is at least 720p. Lastly, YouTube forces lossy compression on all videos even when lossless compression is considerably more sane. For example, the lossless example footage on RGBScaler has a video bitrate of ~100kbps, which is much smaller than YouTube can deliver for lossy pixel art gameplay even for its 720p60 resolution.

### Limitations

You may need to use window mode and reduce the size of the window for the canvas player to work at full framerate effectively due to the inefficiency of using javascript and canvas to scale video. The video player may also run very inefficiently when the source video is AV1 encoded and the system does not support hardware AV1 decode.

In order to hide the source video and only display one video image onscreen all of the video player controls have had to be reimplemented in HTML+JS which is pretty bad for accessibilty standards and definitely still has outstanding bugs.
