import { RGBScaler } from '@tikevin83/react-rgbscaler';
import Head from 'next/head';
import { ChangeEvent, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import useWindowSize from '../../hooks/useWindowSize';

export default function Home() {
  const router = useRouter();
  const initialTasvideosPublication =
    (router.query.tasvideosPublication as string) || '5064';
  const initialPar = router.query.par
    ? parseFloat(router.query.par as string)
    : 1;
  const initialDar = router.query.dar
    ? parseFloat(router.query.dar as string)
    : 1;
  const initialMaskIntensity = router.query.maskIntensity
    ? parseFloat(router.query.maskIntensity as string)
    : 0;
  const initialScanlineIntensity = router.query.scanlineIntensity
    ? parseFloat(router.query.scanlineIntensity as string)
    : 0;
  const [par, setPar] = useState(initialPar);
  const [dar, setDar] = useState(initialDar);
  const [maskIntensity, setMaskIntensity] = useState(initialMaskIntensity);
  const [scanlineIntensity, setScanlineIntensity] = useState(
    initialScanlineIntensity
  );
  const [integerMode, setIntegerMode] = useState(false);
  const [tasvideosPublication, setTasvideosPublication] = useState(
    initialTasvideosPublication
  );
  const windowSize = useWindowSize();

  useEffect(() => {
    setTasvideosPublication(initialTasvideosPublication);
  }, [initialTasvideosPublication]);

  useEffect(() => {
    setPar(initialPar);
  }, [initialPar]);

  useEffect(() => {
    setDar(initialDar);
  }, [initialDar]);

  useEffect(() => {
    setPar(initialPar);
  }, [initialPar]);

  useEffect(() => {
    setMaskIntensity(initialMaskIntensity);
  }, [initialMaskIntensity]);

  useEffect(() => {
    setScanlineIntensity(initialScanlineIntensity);
  }, [initialScanlineIntensity]);

  function updateQueryParams(
    tasvideosPublicationParam: string,
    darParam: number,
    parParam: number,
    maskIntensityParam: number,
    scanlineIntensityParam: number
  ) {
    router.push(
      `/tasvideos?tasvideosPublication=${tasvideosPublicationParam}&dar=${darParam}&par=${parParam}&maskIntensity=${maskIntensityParam}&scanlineIntensity=${scanlineIntensityParam}`,
      undefined,
      { shallow: true }
    );
  }

  function handleParChange(event: ChangeEvent<HTMLInputElement>) {
    updateQueryParams(
      tasvideosPublication,
      dar,
      parseFloat(event.target.value),
      maskIntensity,
      scanlineIntensity
    );
  }

  function handleDarChange(event: ChangeEvent<HTMLInputElement>) {
    updateQueryParams(
      tasvideosPublication,
      parseFloat(event.target.value),
      par,
      maskIntensity,
      scanlineIntensity
    );
  }

  function handleTasvideosPublicationChange(
    event: ChangeEvent<HTMLInputElement>
  ) {
    updateQueryParams(
      event.target.value,
      dar,
      par,
      maskIntensity,
      scanlineIntensity
    );
  }

  function handleMaskIntensityChange(event: ChangeEvent<HTMLInputElement>) {
    updateQueryParams(
      tasvideosPublication,
      dar,
      par,
      parseFloat(event.target.value),
      scanlineIntensity
    );
  }

  function handleScanlineIntensityChange(event: ChangeEvent<HTMLInputElement>) {
    updateQueryParams(
      tasvideosPublication,
      dar,
      par,
      maskIntensity,
      parseFloat(event.target.value)
    );
  }

  return (
    <>
      <Head>
        <title>RGBScaler - Home Page</title>
      </Head>
      <main>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',
            gap: '1em',
          }}
        >
          <h1>RGBScaler</h1>
          <label htmlFor="par-input">PAR</label>
          <input
            type="number"
            style={{ width: '4em' }}
            id="par-input"
            onChange={handleParChange}
            value={par}
            step={0.01}
          />
          <label htmlFor="dar-input">DAR</label>
          <input
            type="number"
            style={{ width: '4em' }}
            id="dar-input"
            onChange={handleDarChange}
            value={dar}
            step={0.01}
          />
          <label htmlFor="maskIntensity">Slot Mask Intensity</label>
          <input
            type="number"
            id="maskIntensity"
            value={maskIntensity}
            min="0.0"
            step="0.05"
            onChange={handleMaskIntensityChange}
            style={{ width: '4em' }}
          />
          <label htmlFor="scanlineIntensity">Scanline Intensity</label>
          <input
            type="number"
            id="scanlineIntensity"
            value={scanlineIntensity}
            min="0.0"
            step="0.05"
            onChange={handleScanlineIntensityChange}
            style={{ width: '4em' }}
          />
          <label htmlFor="integer-checkbox">Integer Mode</label>
          <input
            type="checkbox"
            id="integer-checkbox"
            checked={integerMode}
            onChange={() => {
              setIntegerMode((currentIntegerMode) => !currentIntegerMode);
            }}
          />
          <label htmlFor="tasvideos-publication-input">
            TASVideos Publication
          </label>
          <input
            type="url"
            id="tasvideos-publication-input"
            placeholder="5064"
            value={tasvideosPublication}
            onChange={handleTasvideosPublicationChange}
          />
          <a href="https://gitlab.com/TiKevin83/rgbscaler">
            More info on GitLab
          </a>
          <a
            id="credits"
            href={`http://tasvideos.org/${tasvideosPublication}M`}
          >
            Original TASVideos Publication
          </a>
          <div style={{ flexBasis: '100%', height: 0 }} />
          <RGBScaler
            sourceProps={[
              {
                src: `videos/${tasvideosPublication}M-av1.webm`,
                type: 'video/webm',
              },
              {
                src: `videos/${tasvideosPublication}M-h265.mp4`,
                type: 'video/mp4;codecs="hvc1"',
              },
            ]}
            maxCanvasWidth={windowSize?.width || 0}
            maxCanvasHeight={windowSize?.height || 0}
            volumeBarProps={{ volume: 0.1 }}
            dar={dar !== 1 ? dar : undefined}
            par={par !== 1 ? par : undefined}
            integerScaling={integerMode}
            maskIntensity={maskIntensity}
            scanlineIntensity={scanlineIntensity}
          />
        </div>
      </main>
      <footer />
    </>
  );
}
